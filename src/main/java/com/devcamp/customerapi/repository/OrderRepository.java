package com.devcamp.customerapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerapi.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {

}
